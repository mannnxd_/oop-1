<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("Shaun");
    echo "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

    $frog = new Frog("Buduk");
    echo "Name : " . $frog->name . "<br>"; // "frog"
    echo "Legs : " . $frog->legs . "<br>"; // 4
    echo "Cold blooded : " . $frog->cold_blooded . "<br>"; // "no"
    echo $frog->jump() . "<br><br>";

    $ape = new Ape("Kera Sakti");
    echo "Name : " . $ape->name . "<br>";
    echo "Legs : " . $ape->legs . "<br>";
    echo "Cold blooded : " . $ape->cold_blooded . "<br>";
    echo $ape->yell();
?>